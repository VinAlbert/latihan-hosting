@extends('layout.base')
@section('title','Index Page')
@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class = "mt-3">Project Experiment Laravel</h1>
                <p class="text-monospace">Project khusus untuk pengembangan penggunaan laravel dan experiment project khusus yang ingin diimplementasikan</p>
            </div>
        </div>
    </div>
@endsection
