@extends('layout.mainMhs(#1)')
@section('title','Daftar Mahasiswa')
@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class = "mt-3">Daftar Mahasiswa</h1>
                <table class ="table">
                    <thead class = "thead-dark">
                        <tr>
                            <th scope = "col">#</th>
                            <th scope = "col">Nama</th>
                            <th scope = "col">NIM</th>
                            <th scope = "col">Email</th>
                            <th scope = "col">Jurusan</th>
                            <th scope = "col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($students)== 0)
                            <tr>
                                <td colspan="6">No Data Entry</td>
                            </tr>
                        @endif
                        @foreach($students as $student)
                        <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{ $student->name}}</td>
                            <td>{{ $student->NIM}}</td>
                            <td>{{ $student->email}}</td>
                            <td>{{ $student->jurusan}}</td>
                            <td>
                                <a href="" class = "badge badge-success">edit</a>
                                <a href="" class = "badge badge-danger">delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
