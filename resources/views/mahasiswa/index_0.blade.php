@extends('layout.mainMhs(#1)')
@section('title','Index Page')
@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class = "mt-3">Latihan CRUD Sederhana</h1>
                <p class="text-monospace">Latihan CRUD Data Mahasiswa</p>
                <ul>
                    <li>Template: Boostrap</li>
                    <li>Source: YouTube Web Programming Unpas -> NGOBAR = Laravel #17 - Laravel #20
                </ul>
            </div>
        </div>
    </div>
@endsection
