@extends('layout.mainMhs(#1)')
@section('title','Form Tambah Data')
@section('container')
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h1 class = "mt-3">Form Tambah Mahasiswa</h1>
                <form action = "/parsialDataMhs" method = "post">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="nama" placeholder="Masukkan Nama" name="name" value="{{old('name')}}">
                        @error('name')
                            <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nim">NIM</label>
                        <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" placeholder="Masukkan NIM" name="nim" value="{{old('nim')}}">
                        @error('nim')
                            <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukkan email" name="email" value="{{old('email')}}">
                        @error('email')
                            <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jurusan">Jurusan</label>
                        <input type="text" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="Masukkan jurusan" name="jurusan" value="{{old('jurusan')}}">
                        @error('jurusan')
                            <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah data!</button>
                </form>
            </div>
        </div>
    </div>
@endsection
