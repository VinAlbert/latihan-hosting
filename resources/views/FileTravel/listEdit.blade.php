@extends('layout.main_fileLayout')
@section('title','List File')
@section('container')
    <h1 class = "mt-3">List File</h1>
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @elseif(session('failed'))
        <div class="alert alert-danger">
            {{session('failed')}}
        </div>
    @endif
    <table class ="table">
        <thead class = "thead-dark">
            <tr>
                <th scope = "col">#</th>
                <th scope = "col">Title</th>
                <th scope = "col">Action</th>
            </tr>
        </thead>
        <tbody>
            @if (count($files)== 0)
                <tr>
                    <td colspan="3">No Data Entry</td>
                </tr>
            @endif
            @foreach($files as $file)
            <tr>
                <td>{{$loop->iteration }}</td>
                <td>{{$file->title}}</td>
                <td>
                    <a href="/edit/{{$file->title}}" class = "badge badge-success">edit</a>
                    <a href="/delete/{{$file->title}}" class = "badge badge-danger">delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection