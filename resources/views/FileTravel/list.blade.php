@extends('layout.main_fileLayout')
@section('title','List File')
@section('container')
    <h1 class = "mt-3">List File</h1>
    <ul class="list-group">
        @foreach ($files as $file)
        <li class="list-group-item d-flex justify-content-between align-items-center">
            {{$file->title}}
            <a href = "/listFile/{{$file->title}}" class="badge badge-info">detail</a>
        </li>
        @endforeach
    </ul>
@endsection