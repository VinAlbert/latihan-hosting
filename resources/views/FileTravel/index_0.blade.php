@extends('layout.main_fileLayout')
@section('title','Experiment File')
@section('container')
<h1 class = "mt-3">File Handling PHP</h1>
<p class="text-monospace">Experiment file handling untuk kepentingan tugas besar Pemrogaman Berbasis Platform</p>
<ul>
    <li>Upload File dan simpan file path (Clear)</li>
        <ul type = "square">
            <li>Lokasi upload file</li>
            <li>Kondis folder sudah ada/belum</li>
            <li>Manipulasi nama file</li>
            <li>Research: upload multiple file max: 5</li>
            <li>simpan di db</li>
        </ul>
    <li>Retrieve File dan .txt</li>
        <ul type = "square">
            <li>Lokasi file path dari db</li>
            <li>disesuaikan dengan template view nya (read file dan img source)</li>
        </ul>
    <li>Update File dan .txt</li>
        <ul type = "square">
            <li>Menampilkan file/preview data lama</li>
            <li>Ubah data</li>
        </ul>
    <li>Delete File dan .txt</li>
        <ul type = "square">
            <li>Show list per type dan lihat title</li>
            <li>Remove directory dengan nama titlenya</li>
            <li>mengimplementasi soft delete</li>
        </ul>
</ul>
<p>Keterangan lain: </p>
<ul>
    <li>Tabel: files</li>
    <ul type = "square">
        <li>type = (Destination, Hotels, Cullinary)</li>
        <li>name = (nama kuliner, destinasi, atau hotel)</li>
        <li>main_pic = (picture utama yang dijadikan pajangan. Qty: 1)</li>
        <li>description = (file path tempat document .txt disimpan)</li>
        <li>gallery = (picture yang dipajang di dalam gallery. Qty: max 6, min 3)</li>
        <li>Contact = (nomor kontak, pajang max 3, min 1)</li>
        <li>Price = (txt dengan list))</li>
    </ul>
</ul>
@endsection