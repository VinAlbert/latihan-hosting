@extends('layout.main_fileLayout')
@section('title','Coba Read File')
@section('container')
    @foreach($file as $file)
    <h1>{{$file->title}}</h1>
    <img src = "{{ asset($file->main_pic) }}">
    <br>
    @php
        $myfile = fopen($file->desc, "r") or die("Unable to open file!");
        // Output one line until end-of-file
        while(!feof($myfile)) {
            echo fgets($myfile)."<br>";
        }
        fclose($myfile);
    @endphp
    <br>
    <h2>Price</h2>
    @php
        $myfile = fopen($file->price, "r") or die("Unable to open file!");
        // Output one line until end-of-file
        while(!feof($myfile)) {
            echo fgets($myfile)."<br>";
        }
        fclose($myfile);
    @endphp
    <br>
    @endforeach
    @foreach($galleries as $gal)
        <div class="card" style="width: 18rem;">
        <img src="{{ asset($gal->gallery) }}" class="card-img-top" alt = "{{$gal->name}}">
            <div class="card-body">
                <h1 class="card-text" text-align = "center">{{$gal->name}}</h1>
            </div>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;
    @endforeach

    @foreach($contacts as $con)
        <p>Main Contact: {{$con->phone}}</p>
        @if($con->instagram != null)
            Instagram: <a href = "{{url('https://www.instagram.com/$con->instagram')}}" target = "_blank">{{$con->instagram}}</a>
        @endif
        @if($con->facebook != null)
            <p>Facebook: {{$con->facebook}}</p>
        @endif
        @if($con->twitter != null)
            <p>Twitter: {{$con->twitter}}</p>
        @endif
        @if($con->google != null)
            <p>Google: {{$con->google}}</p>
        @endif
        @if($con->pinterest != null)
            <p>Pinterest: {{$con->pinterest}}</p>
        @endif
        @if($con->linkedin != null)
            <p>Linked-In: {{$con->linkedin}}</p>
        @endif
        
    @endforeach
@endsection