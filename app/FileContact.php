<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileContact extends Model
{
    protected $table = 'files_contact';
}
