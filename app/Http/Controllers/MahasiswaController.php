<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mainMenu(){
        return view('mahasiswa.index_0');
    }

    public function index()
    {
        $students = Student::all();
        return view('mahasiswa.index',compact('students'));
    }

    public function partialIndex()
    {
        $students = Student::all();
        return view('mahasiswa.partialInfo',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'nim' => 'required|size:7',
            'email' => 'required',
            'jurusan' => 'required'
        ]);
    //Store Data metode pertama
        // $student = new Student;
        // $student->name = $request->name;
        // $student->NIM = $request->nim;
        // $student->email = $request->email;
        // $student->jurusan = $request->jurusan;

        // $student->save();

        
        //Store data metode kedua (kendala mass assignment)
            // Student::create([
            //     'name' => $request->name,
            //     'NIM' => $request->nim,
            //     'email' => $request->email,
            //     'jurusan' => $request->jurusan,
            // ]);
            
        //Store data metode ketiga (mass assignment dengan fillabel pada model)
            Student::create($request->all()); //hanya ambil yang didalam fillable atau di luar guarded
            return redirect('/parsialDataMhs')->with('status','Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('mahasiswa.detail',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view ('mahasiswa.edit',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'nim' => 'required|size:7',
            'email' => 'required',
            'jurusan' => 'required'
        ]);

        $id = $student->id;
        Student::where('id',$student->id)
                ->update([
                    'name' => $request->name,
                    'NIM' => $request->nim,
                    'email' => $request->email,
                    'jurusan' => $request->jurusan,
                ]);

                return redirect('/parsialDataMhs')->with('status',"Data dengan id $id berhasil diubah");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $id = $student->id;
        Student::destroy($student->id);
        return redirect('/parsialDataMhs')->with('status',"Data dengan id $id berhasil dihapus"); 
    }
}
