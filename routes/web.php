<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

//=============================================================================================================
//Experiment Laravel Upload File
//=============================================================================================================

Route::get('/fileHandling', 'PagesController@fileHandling');

Route::get('/uploadFile', 'FilesController@create');
Route::post('/uploadFile', 'FilesController@store');

Route::get('/listFile', 'FilesController@index');
Route::get('/listFile/{file}', 'FilesController@show');

Route::get('/edit', 'FilesController@index_edit');
Route::get('/edit/{title}', 'FilesController@edit');
Route::patch('/edit/{title}/{type}', 'FilesController@update');

Route::delete('/edit/{title}/{name}', 'FilesController@destroyGal');

//=============================================================================================================
//Mahasiswa(#1) Exercise Laravel
//=============================================================================================================

Route::get('/latihanMhs', 'MahasiswaController@mainMenu');

//Mahasiswa dengan seluruh data
Route::get('/mahasiswa', 'MahasiswaController@index');

//Mahasiswa dengan sebagian data
Route::get('/parsialDataMhs/create', 'MahasiswaController@create');
Route::post('/parsialDataMhs', 'MahasiswaController@store');

Route::get('/parsialDataMhs', 'MahasiswaController@partialIndex');
Route::get('/parsialDataMhs/{student}', 'MahasiswaController@show');

Route::get('/parsialDataMhs/{student}/edit', 'MahasiswaController@edit');
Route::patch('/parsialDataMhs/{student}', 'MahasiswaController@update');

Route::delete('/parsialDataMhs/{student}','MahasiswaController@destroy');
